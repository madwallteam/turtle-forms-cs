﻿using Nakov.TurtleGraphics;
using System;
using System.Windows.Forms;

namespace turtle_forms_cs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ResetBtn_Click(object sender, EventArgs e)
        {
            Turtle.Reset();      
        }

        private void DrawBtn_Click(object sender, EventArgs e)
        {
            Turtle.Delay = 150;
            Turtle.PenSize = 8;

            Turtle.Rotate(90);
            Turtle.Forward(100);
            Turtle.Rotate(-120);
            Turtle.Forward(100);
            Turtle.Rotate(-120);
            Turtle.Forward(100);
            Turtle.Rotate(-120);

            Turtle.ShowTurtle = false;
        }
    }
}